package com.example.ts1seleniumtest;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class RegistrationPage {

  public SelenideElement firstname;

  public SelenideElement lastname;

  public SelenideElement telephone;

  public SelenideElement email;

  public SelenideElement pass;

  public SelenideElement submitButton;

  public RegistrationPage (
      String firstname, String lastname,
      String email, String pass,
      String tel, boolean subscribeToNewsLetter
  ) {
    this.firstname = setInputWithNameToText("firstname", firstname);
    this.lastname = setInputWithNameToText("lastname", lastname);
    this.email = setInputWithNameToText("email", email);
    this.telephone = setInputWithNameToText("telephone", tel);

    this.pass = setInputWithNameToText("password", pass);
    setInputWithNameToText("confirm", pass);


    var subscription= $$(By.name("newsletter"));
    subscription.get( subscribeToNewsLetter ? 1 : 0 ).click();

    $(By.name("agree")).click();

    this.submitButton = $(".pull-right > input + input");
  }

  private SelenideElement setInputWithNameToText(String name, String value) {
    return  $(By.name(name)).setValue(value);
  }

  /**
   * Sends form info to the server.
   */
  public void submitForm() {
    this.submitButton.click();
  }

  public void showInfo() {
    System.out.println("Form fields:");
    System.out.println(this.firstname.getValue());
    System.out.println(this.lastname.getValue());
    System.out.println(this.email.getValue());
    System.out.println(this.telephone.getValue());
    System.out.println("=====================================");
    System.out.println(this.pass.getValue());
  }


}
