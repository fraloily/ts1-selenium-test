package com.example.ts1seleniumtest;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage {

  public String address = "https://demo.opencart.com/";
  public SelenideElement seeAllToolsButton = $("a.wt-button_mode_primary");
  public SelenideElement toolsMenu = $x(
      "//div[@data-test='main-menu-item' and @data-test-marker = 'Developer Tools']");
  public SelenideElement searchButton = $("[data-test='site-header-search-action']");

  public void goToRegistrationPage() {
    viewLoginAndRegistration();
    $(".dropdown-menu-right > li > a").click();
  }

  public void loginFromMain(String email, String pass) {
    viewLoginAndRegistration();
    $(".dropdown-menu-right > li + li > a").click();
    $(By.name("email")).setValue(email);
    $(By.name("password")).setValue(pass);
    $(".well > form > input").click();
  }

  private void viewLoginAndRegistration() {
    $("li.dropdown > .dropdown-toggle").click();
  }
}
