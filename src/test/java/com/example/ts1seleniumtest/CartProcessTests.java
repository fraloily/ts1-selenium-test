package com.example.ts1seleniumtest;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.*;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.ErrorsCollector;
import com.codeborne.selenide.logevents.SelenideLogger;
import java.util.Objects;
import org.junit.jupiter.api.BeforeAll;
import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;

public class CartProcessTests {

  MainPage main;


  @BeforeAll
  public static void setUpAll() {
    Configuration.browserSize = "1280x1000";
    SelenideLogger.addListener("allure", new ErrorsCollector());
  }

  @BeforeEach
  public void setUpEach() {

    this.main = new MainPage();
    open(main.address);
    //main.loginFromMain( "albert.palych@gmail.com", "matebal#");
  }

  public void login(String name, String password) {
    main.loginFromMain(name, password);
  }


    // 1 - 2 - 3 - 4 - 5 - 6
  @Test
  public void openMain_OpenProductPage_AddToCart_OpenCart_OutOfStock() {
    openProductPage("MacBook");
    addProductToCartFromProductPage(200);
    openCart();
    assertNotInStock();
  }

  // 1 - 4 - 5 - 6
  @Test
  public void openMain_addProductToCart_OpenCart_OutOfStock() {
   addProductToCartFromProductCard("MacBook");
   openCart();
  }

  // 1 - 4 - 5 - 7 - 8
  @ParameterizedTest
  @CsvSource({"HP LP3065"})
  public void openMain_addProductToCart_OpenCart_Checkout_Finished(String item) {
    $(By.name("search")).setValue(item).parent().lastChild().click();
    $(".product-thumb > .image > a > img")
        .shouldHave(Condition.attribute("title", item))
        .parent().parent().parent().lastChild().$("button > .fa-shopping-cart").click();
    addProductToCartFromProductPage(1);
    openCart();
    $("div.buttons > .pull-right > a").click();
    assertEquals(Selenide.title(), "Shopping Cart");
  }

  // 1 - 4 - 5 - 10 - 11 - 8
  //public void openMain_addProductToCart_

  //B
  public void openProductPage(String item) {
    $(".product-thumb > .image > a > img").shouldHave(Condition.attribute("title", item)).click();
    assertEquals(item, "MacBook");
  }

  public void assertEditedSuccessfully() {
    $("div.alert-success").shouldHave(Condition.exist);
  }

  public void addProductToCartFromProductPage(int number) {
    $("#input-quantity").setValue(String.valueOf(number));
    $("#button-cart").click();
    //assertEditedSuccessfully();
  }

  public void assertNotInStock() {
    $("div.alert-danger").should(Condition.exist);
  }

  public void addProductToCartFromProductCard(String item) {
    $(".product-thumb > .image > a > img")
        .shouldHave(Condition.attribute("title", item))
        .parent().parent().parent().lastChild().$("button > .fa-shopping-cart").click();
    $("div.alert.alert-success").should(Condition.exist);
  }

  public static void logout() {
    $(".fa-user").click();
    $("ul.dropdown-menu.dropdown-menu-right").lastChild().click();
  }

  public void openCart() {
    $(".fa-shopping-cart").click();
  }

}
