package com.example.ts1seleniumtest;

public class User {
  public String name = "John";
  public String surname = "Doe";
  public String password = "johndoeAFDpass123#";
  public String email = "illa.fralou@gmail.com";

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
