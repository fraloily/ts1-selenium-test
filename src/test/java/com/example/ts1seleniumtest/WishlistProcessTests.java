package com.example.ts1seleniumtest;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;

import static org.junit.jupiter.api.Assertions.*;
import static com.codeborne.selenide.Selenide.*;

public class WishlistProcessTests {

  MainPage main;

  @BeforeAll
  public static void setUpAll() {
    Configuration.browserSize = "1280x800";
    SelenideLogger.addListener("allure", new AllureSelenide());

  }

  @AfterEach
  public void cleanup() {
    logout();
  }

  @BeforeEach
  public void setUpMain() {
    this.main = new MainPage();
    open(main.address);
  }

  /*
  @BeforeEach
  public void setUp() {
    open("https://www.jetbrains.com/");
  }

   */


  public static void logout() {
    $(".fa-user").click();
    $("ul.dropdown-menu.dropdown-menu-right").lastChild().click();
  }

  @ParameterizedTest
  @CsvSource({
      "JognnnnnGold, PalychH, albertik.palychick@email.com, matebal#, +429233525235",
      "Turocek, Vasylsdfsdych, tsadurok.bolhsoj@gmail.cz, jasosBiba#, +1958324834234",
      "sadfDarova, sdfParni, fjsdf.parni@dasfasdf.cz, daroadfsda#, +42323444234234"
  })
  public void register(String first, String last, String email, String pass, String tel) {
    //Go to registration page and check if it is registration
    main.goToRegistrationPage();
    assertEquals("Register Account", title());

    //Fills the form with the info
    var registration = new RegistrationPage(
        first, last, email, pass, tel, false
    );

    //And submits form
    registration.submitForm();

    assertEquals("Your Account Has Been Created!", title());
  }

  // 1-2-10-15
  @ParameterizedTest
  @CsvSource({
      "albert.palych@gmail.com, matebal#",
      "fraloily_test@test.com, testtest##",
      "darova.parni@dasfasdf.cz, daroadfsda#"
  })
  public void login_AddMacbookToWishList_DeleteFromWishList_End(String email, String password) {
    login_AddMackbookToWishList(email, password);


    deleteItemFromWishlist();
  }

  //1 - 2 - 11 - 18 -15
  @Test
  public void login_AddMackbookToWishList_AddAppleMonitor_deleteFirstItemOnWishList_end() {
    login_AddMackbookToWishList( "fraloily_test@test.com", "testtest##");

    addAppleMonitorToWishList();

    deleteItemFromWishlist();
  }

  //1 - 2 - 10 - 14 - 18 - 14 - 9 - 13
  @Test
  public void login_AddMackBookToWishList_deleteFirstItemOnWishList_AddAppleMonitor_deleteFirstItemOnWishList_addAppleMonitor_addTablet_end() {
    login_AddMackbookToWishList( "fraloily_test@test.com", "testtest##");
    deleteItemFromWishlist();
    addAppleMonitorToWishList();
    deleteItemFromWishlist();
    addAppleMonitorToWishList();
    addTabletToWishList();
  }

  //1 - 2 - 11 - 9 - 13
  @Test
  public void login_AddMackBootToWishList_AddAppleMonitor_AddTablet_end() {
    login_AddMackbookToWishList( "fraloily_test@test.com", "testtest##");
    addAppleMonitorToWishList();
    addTabletToWishList();
  }

  // 1 - 2 - 16
  @Test

  public void login_AddMackbookToWishList() {
    login_AddMackbookToWishList( "fraloily_test@test.com", "testtest##");
  }

  // 1 - 2 - 11 - 12
  @Test
  public void login_AddMackbookToWishList_AddAppleMonitor() {
    login_AddMackbookToWishList( "fraloily_test@test.com", "testtest##");
    addAppleMonitorToWishList();
  }

  // 1 - 2 - 10 - 14 - 12
  @Test
  public void login_AddMackBookToWishList_deleteFirstItemOnWishList_AddAppleMonitor() {
    login_AddMackbookToWishList( "fraloily_test@test.com", "testtest##");
    deleteItemFromWishlist();
    addAppleMonitorToWishList();
  }

  public void checkWishListSize() {

  }

  public void addTabletToWishList() {
    searchAddItemToWishList("Samsung Galaxy Tab 10.1");
  }

  public void deleteItemFromWishlist() {
    navigateToWishListAndDeleteItem();
    assertWishListEditedSuccessfully();
  }

  private void login_AddMackbookToWishList(String name, String pass) {
    login(name, pass);
    assertLogin();
    addMacBookToWishlist();
  }

  public void addAppleMonitorToWishList() {
    searchAddItemToWishList("Apple Cinema 30\"");
  }
  public void addMacBookToWishlist() {
   searchAddItemToWishList("MacBook");
  }

  private void searchAddItemToWishList(String item) {
    searchFor(item);
    itemPageFound(item);

    addToFavorite();
    assertWishListEditedSuccessfully();
  }

  public void assertLogin() {
    assertEquals("My Account", Selenide.title());
  }


  public void login(String name, String password) {
    main.loginFromMain(name, password);
  }

  public void navigateToWishListAndDeleteItem() {
    $("#wishlist-total").click();
    assertEquals("My Wish List", Selenide.title());
    $("#content > div > table > tbody > tr").lastChild().lastChild().click();
  }

  public void itemPageFound(String item) {
    $("#content > div > div > h1").shouldHave(Condition.text(item));
  }

  public void assertWishListEditedSuccessfully() {
    $("div.alert-success").shouldHave(Condition.exist);
  }



//  public void searchForMacBook() {
//    $(By.name("search"))
//        .setValue("MacBook")
//        .parent().lastChild().click();
//    $("img").shouldHave(Condition.attribute("title", "MacBook")).click();
//  }

  public void searchFor(String query) {
    $(By.name("search"))
        .setValue(query)
        .parent().lastChild().click();
    $("img").shouldHave(Condition.attribute("title", query)).click();
  }

  public void addToFavorite() {
    $("button > .fa-heart").click();
  }

}
